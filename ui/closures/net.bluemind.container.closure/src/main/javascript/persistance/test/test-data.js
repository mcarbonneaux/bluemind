goog.provide('net.bluemind.container.persistance.testdata');

net.bluemind.container.persistance.testdata.Entry1 = {
	'uid' : 'test',
	'name' : 'dispName',
	'externalId' : 'extIdTest',
	'value' : {
		"fake" : "value"
	}
};