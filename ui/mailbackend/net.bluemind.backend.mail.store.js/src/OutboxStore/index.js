import * as actions from "./actions.js";

export default {
    namespaced: true,
    actions
};
