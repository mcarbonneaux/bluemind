export {default as EventTarget} from './EventTarget';
export {default as Event} from './Event';