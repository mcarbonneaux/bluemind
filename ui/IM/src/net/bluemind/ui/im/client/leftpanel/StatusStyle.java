package net.bluemind.ui.im.client.leftpanel;

import com.google.gwt.resources.client.CssResource;

public interface StatusStyle extends CssResource {

	public String status();

	public String statusIM();

	public String statusPhone();

	public String statusOffline();

	public String statusPhoneOffline();

	public String statusAvailable();

	public String statusPhoneAvailable();

	public String statusAway();

	public String statusPhoneAway();

	public String statusBusy();

	public String statusPhoneRinging();
}
