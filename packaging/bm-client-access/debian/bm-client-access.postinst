#!/bin/bash
#BEGIN LICENSE
#
# Copyright © Blue Mind SAS, 2012-2016
#
# This file is part of BlueMind. BlueMind is a messaging and collaborative
# solution.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of either the GNU Affero General Public License as
# published by the Free Software Foundation (version 3 of the License).
#
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#
# See LICENSE.txt


set -e

# Source debconf library.
. /usr/share/debconf/confmodule

generateDhParam() {
    if [ -e /etc/nginx/bm_dhparam.pem ]; then
        chmod 640 /etc/nginx/bm_dhparam.pem
        return
    fi
    
    char=("-" "\\" "|" "/")
    charCount=0;
    elapsedTime=0;
    
    openssl dhparam -out /etc/nginx/bm_dhparam.pem 2048 > /dev/null 2>&1 &
    while $(kill -0 $! > /dev/null 2>&1); do
        echo -en "\rGenerate 2048 dhparams. This may take some time: "${char[$charCount]}" ("$((elapsedTime/2))"s)"
        
        elapsedTime=$((elapsedTime+1))
        charCount=$(((charCount+1)%4))
        sleep 0.5
    done
    
    chmod 640 /etc/nginx/bm_dhparam.pem
    
    echo -e "\n"
}


echo "Install/Upgrade BlueMind nginx virtual host"
if [ -e /usr/share/doc/bm-client-access/bm-client-access.gz ]; then
    zcat /usr/share/doc/bm-client-access/bm-client-access.gz > /etc/nginx/sites-available/bm-client-access
else
    cp -f /usr/share/doc/bm-client-access/bm-client-access /etc/nginx/sites-available/
fi

if [ -e /usr/share/doc/bm-client-access/bm-client-access-without-password.gz ]; then
    zcat /usr/share/doc/bm-client-access/bm-client-access-without-password.gz > /etc/nginx/sites-available/bm-client-access-without-password
else
    cp -f /usr/share/doc/bm-client-access/bm-client-access-without-password /etc/nginx/sites-available/
fi

echo "Install/Upgrade BlueMind nginx mail configuration"
cp -f /usr/share/doc/bm-client-access/global.d/bm-mail-proxy.conf /etc/nginx/global.d/

echo "Install bm-webmail nginx configuration"
if [ ! -e /etc/bm-webmail/nginx-webmail.conf ]; then
  cp -f /usr/share/doc/bm-client-access/bm-webmail/nginx-webmail.conf /etc/bm-webmail
fi
if [ ! -e /etc/bm-webmail/bm-filehosting.conf ]; then
  cp -f /usr/share/doc/bm-client-access/bm-webmail/bm-filehosting.conf /etc/bm-webmail
fi

if [ ! -e /etc/bm-eas/bm-eas-nginx.conf ]; then
  echo "Install bm-eas nginx configuration"
  cp -f /usr/share/doc/bm-client-access/bm-eas/bm-eas-nginx.conf /etc/bm-eas
fi

if [ ! -e /etc/bm-eas/bm-upstream-eas.conf ]; then
  echo "Install bm-eas nginx upstream configuration"
  cp -f /usr/share/doc/bm-client-access/bm-eas/bm-upstream-eas.conf /etc/bm-eas
fi

if [ ! -e /etc/bm-mapi/bm-upstream-mapi.conf ]; then
  echo "Install bm-mapi nginx upstream configuration"
  cp -f /usr/share/doc/bm-client-access/bm-mapi/bm-upstream-mapi.conf /etc/bm-mapi
fi

if [ -f "/etc/bm/bm.ini" ]; then
  externalurl=`cat /etc/bm/bm.ini | grep "^external-url" | sed -e 's/ //g' | cut -d'=' -f2`
else
  externalurl="configure.your.external.url"
fi

echo "server_name $externalurl;" > /etc/nginx/bm-servername.conf
echo "set \$bmexternalurl $externalurl;" > /etc/nginx/bm-externalurl.conf

pushd /etc/nginx/sites-enabled
rm -f bm-client-access*
if [ -f /etc/nginx/sw.htpasswd ]; then
    cp -f ../sites-available/bm-client-access .
else
    cp -f ../sites-available/bm-client-access-without-password .
fi
popd

if [ "${externalurl}" = "configure.your.external.url" ]; then
    /usr/share/bm-client-access/bin/createcert.sh ${externalurl}
fi

generateDhParam

echo "Restart nginx service"
invoke-rc.d bm-nginx stop 0>/dev/null 1>/dev/null 2>/dev/null 3>/dev/null || true
# see /etc/init.d/nginx@restart
sleep 1
invoke-rc.d bm-nginx start 0>/dev/null 1>/dev/null 2>/dev/null 3>/dev/null || true

exit 0
