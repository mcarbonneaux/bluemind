/* BEGIN LICENSE
 * Copyright © Blue Mind SAS, 2012-2016
 *
 * This file is part of BlueMind. BlueMind is a messaging and collaborative
 * solution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU Affero General Public License as
 * published by the Free Software Foundation (version 3 of the License).
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See LICENSE.txt
 * END LICENSE
 */
package net.bluemind.eas.timezone;

import java.nio.ByteOrder;
import java.nio.charset.Charset;

import org.vertx.java.core.json.JsonObject;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.handler.codec.base64.Base64;

public class EASTimeZone {

	public final int bias;
	public final String standardName;
	public final SystemTime standardDate;
	public final int standardBias;
	public final String daylightName;
	public final SystemTime daylightDate;
	public final int daylightBias;

	public EASTimeZone(int bias, String standardName, SystemTime standardDate, int standardBias, String daylightName,
			SystemTime daylightDate, int daylightBias) {
		this.bias = bias;
		this.standardName = standardName;
		this.standardDate = standardDate;
		this.standardBias = standardBias;
		this.daylightName = daylightName;
		this.daylightDate = daylightDate;
		this.daylightBias = daylightBias;
	}

	public String toBase64() {
		ByteBuf out = Unpooled.buffer().order(ByteOrder.LITTLE_ENDIAN);
		out.writeInt(bias);
		byte[] utf = standardName.getBytes(Charset.forName("utf-16le"));
		out.writeBytes(utf);
		out.writeZero(64 - utf.length);
		standardDate.writeTo(out);
		out.writeInt(standardBias);

		utf = daylightName.getBytes(Charset.forName("utf-16le"));
		out.writeBytes(utf);
		out.writeZero(64 - utf.length);
		daylightDate.writeTo(out);
		out.writeInt(daylightBias);

		ByteBuf encoded = Base64.encode(out, false);
		String ascii = encoded.toString(Charset.forName("ascii"));
		return ascii;
	}

	public JsonObject toJson() {
		JsonObject ret = new JsonObject();
		ret.putNumber("bias", bias);
		ret.putString("standardName", standardName);
		ret.putObject("standardDate", standardDate.toJson());
		ret.putNumber("standardBias", standardBias);
		ret.putString("daylightName", daylightName);
		ret.putObject("daylightDate", daylightDate.toJson());
		ret.putNumber("daylightBias", daylightBias);
		return ret;
	}

	public String toString() {
		return toJson().encodePrettily();
	}

}
