/**
 * BEGIN LICENSE
 * Copyright © Blue Mind SAS, 2012-2016
 *
 * This file is part of BlueMind. BlueMind is a messaging and collaborative
 * solution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU Affero General Public License as
 * published by the Free Software Foundation (version 3 of the License).
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See LICENSE.txt
 * END LICENSE
 */

/* Bm autocomplete source */
Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");
Components.utils.import("resource://gre/modules/Services.jsm");
if (ChromeUtils.generateQI) {
    //TB >= 65
    Components.utils.import("resource:///modules/MailServices.jsm");
} else {
    Components.utils.import("resource:///modules/mailServices.js");
}
Components.utils.import("resource://bm/bmUtils.jsm");
Components.utils.import("resource://bm/core2/BMFolderHome.jsm");
Components.utils.import("resource://bm/core2/BMAuthService.jsm");

const ACR = Components.interfaces.nsIAutoCompleteResult;
const nsIAbAutoCompleteResult = Components.interfaces.nsIAbAutoCompleteResult;

function BmAutocompleteResult(aSearchString) {
    this._searchResults = new Array();
    this.searchString = aSearchString;
}

BmAutocompleteResult.prototype = {
    QueryInterface: ChromeUtils.generateQI ? 
        ChromeUtils.generateQI([Components.interfaces.nsIAbAutoCompleteResult])
        : XPCOMUtils.generateQI([Components.interfaces.nsIAbAutoCompleteResult]),
    _searchResults: null,
    
    // nsIAutoCompleteResult
    searchString: null,
    searchResult: ACR.RESULT_NOMATCH,
    defaultIndex: -1,
    errorDescription: null,
    
    get matchCount() {
        return this._searchResults.length;
    },
    getValueAt: function getValueAt(aIndex) {
        return this._searchResults[aIndex].value;
    },
    getLabelAt: function getLabelAt(aIndex) {
        return this.getValueAt(aIndex);
    },
    getCommentAt: function getCommentAt(aIndex) {
        return this._searchResults[aIndex].comment;
    },
    getStyleAt: function getStyleAt(aIndex) {
        return "local-abook";
    },
    getImageAt: function getImageAt(aIndex) {
        return "";
    },
    getFinalCompleteValueAt: function(aIndex) {
        return this.getValueAt(aIndex);
    },
    removeValueAt: function removeValueAt(aRowIndex, aRemoveFromDB) {},

    // nsIAbAutoCompleteResult
    getCardAt: function getCardAt(aIndex) {
        return this._searchResults[aIndex].card;
    },
    getEmailToUse: function getEmailToUse(aIndex) {
        return this._searchResults[aIndex].emailToUse;
    },
};

function BmAutocompleteSearch() {
}

BmAutocompleteSearch.prototype = {
    classDescription: "BmAutocompleteSearch XPCOM Component",
    classID: Components.ID("{ddb5a551-9df4-4e2b-8f79-b9e16d2c28f5}"),
    contractID: "@mozilla.org/autocomplete/search;1?name=bm-search",
    QueryInterface: ChromeUtils.generateQI ?
        ChromeUtils.generateQI([Components.interfaces.nsIAutoCompleteSearch,
                                Components.interfaces.nsIAbDirectorySearch])
        : XPCOMUtils.generateQI([Components.interfaces.nsIAutoCompleteSearch,
                                Components.interfaces.nsIAbDirectorySearch]),
    _parser: MailServices.headerParser,
    _abManager: MailServices.ab,
    _logger: Components.classes["@blue-mind.net/logger;1"].getService().wrappedJSObject.getLogger("bmAutocompleteSearch: "),
    _result: null,
    _fullString: null,
    _directories: new HashMap(),

    startSearch: function(searchString, searchParam, previousResult, listener) {
        this._logger.debug("search: " + searchString);
        this._result = new BmAutocompleteResult(searchString);
        if (!searchString || /,/.test(searchString)) {
            this._result.searchResult = ACR.RESULT_IGNORED;
            listener.onSearchResult(this, this._result);
            return;
        }
        this.stopSearch();
        this._listener = listener;
        this._fullString = searchString.toLocaleLowerCase();
        this._searchBlueMind();
    },
    /* Start searchs in remote dirs */
    _searchBlueMind: function() {
        let uris = this._getDirectoriesUris();
        let domainAbFound = false;
        for (let uri of uris) {
            if (!domainAbFound) {
                domainAbFound = bmUtils.getBoolPref(uri + ".bm-domain-ab", false);
            }
            let searchUri = uri + "?(or(PrimaryEmail,bw," + this._fullString + ")(SecondEmail,bw," + this._fullString + "))";
            let directory = MailServices.ab.getDirectory(searchUri);
            if (directory) {
                this._directories.put(uri, directory);
            }
        }
        if (!domainAbFound) {
            this._directories.put("FakeRemoteGroupsDir", new FakeRemoteGroupsDir(this._fullString));
        }
        for (let dir of this._directories.values()) {
            dir.wrappedJSObject.autoCompleteSearch(this);
        }
    },
    _getDirectoriesUris: function() {
        let uris = [];
        let it = MailServices.ab.directories;
        while (it.hasMoreElements()) {
            let directory = it.getNext();
            if (directory instanceof Components.interfaces.nsIAbDirectory) {
                let uri = directory.URI;
                this._logger.debug("_getDirectoriesUris uri:" + uri);
                let id = bmUtils.getCharPref(uri + ".bm-id", null);
                if (id && uri.indexOf("bmdirectory://") == 0) {
                    uris.push(uri);
                }
            }
        }
        return uris;
    },
    _checkDuplicate: function(card, emailAddress) {
        let lcEmailAddress = emailAddress.toLocaleLowerCase();
        return this._result._searchResults.some(function(result) {
            return result.value.toLocaleLowerCase() == lcEmailAddress;
        });
    },
    _addToResult: function(card, dirName) {
        let mbox = this._parser.makeMailboxObject(card.displayName,
        card.isMailList ? card.getProperty("Notes", "") || card.displayName :
                        card.primaryEmail);
        if (!mbox.email) return;
        let emailAddress = mbox.toString();
        // If it is a duplicate, then just return and don't add it. The
        // _checkDuplicate function deals with it all for us.
        if (this._checkDuplicate(card, emailAddress)) return;
        // Find out where to insert the card.
        let insertPosition = 0;
        // Next sort on full address
        while (insertPosition < this._result._searchResults.length &&
               emailAddress > this._result._searchResults[insertPosition].value)
            ++insertPosition;
        this._result._searchResults.splice(insertPosition, 0, {
            value: emailAddress,
            card: card,
            comment: dirName
        });
    },
    stopSearch: function() {
        if (this._listener) {
            for (let dir of this._directories.values()) {
                dir.wrappedJSObject.stopSearch();
            }
            this._listener = null;
        }
    },
    onSearchFoundCard: function onSearchFoundCard(aCard, aUri) {
        this._logger.debug("onSearchFoundCard in: " + aUri);
        if (!this._listener) return;
        this._addToResult(aCard, this._directories.get(aUri).dirName);
    },
    onSearchFinished: function onSearchFinished(aResult, aErrorMsg, aUri) {
        this._logger.debug("onSearchFinished in: " + aUri);
        if (!this._listener) return;
        if (aResult == Components.results.NS_OK) {
            if (this._result.matchCount) {
                this._result.searchResult = ACR.RESULT_SUCCESS;
                this._result.defaultIndex = 0;
            } else {
                this._result.searchResult = ACR.RESULT_NOMATCH;
            }
        } else if (aResult == Components.results.NS_ERROR_FAILURE) {
            this._result.searchResult = ACR.RESULT_FAILURE;
            this._result.defaultIndex = 0;
        }
        this._directories.remove(aUri);
        this._logger.debug("dirs searching: " + this._directories.keys().length);
        if (this._directories.keys().length == 0) {
            this._listener.onSearchResult(this, this._result);
            this._listener = null;
        }
    }
};

function FakeRemoteGroupsDir(aFullString) {
    let loader = Components.classes["@mozilla.org/moz/jssubscript-loader;1"].getService(Components.interfaces.mozIJSSubScriptLoader);
    loader.loadSubScript("chrome://bm/content/core2/client/DirectoryClient.js");
    this._fullString = aFullString;
    this.wrappedJSObject = this;
}

FakeRemoteGroupsDir.prototype = {
    _logger: Components.classes["@blue-mind.net/logger;1"].getService().wrappedJSObject.getLogger("bmAutocompleteGroupSearch: "),
    get URI() {
        return "FakeRemoteGroupsDir";
    },
    get dirName() {
        return "";
    },
    _searchListener: null,
    _searching: false,
    autoCompleteSearch: function(aSearchListener) {
        this._searchListener = aSearchListener;
        let user = {};
        let pwd = {};
        let srv = {};
        if (bmUtils.getSettings(user, pwd, srv, true)) {
            let self = this;
            let result = BMAuthService.login(srv.value, user.value, pwd.value);
            result.then(function(logged) {
                self._searching = true;
                let client = new DirectoryClient(srv.value, logged.authKey, logged.authUser.domainUid);
                return client.search({"kindsFilter": ["GROUP"], "nameOrEmailFilter": self._fullString, "size": 10});
            }).then(function(listResult) {
                for (let iv of listResult.values) {
                    if (iv.value.email) {
                        let card = Components.classes["@mozilla.org/addressbook/cardproperty;1"]
                                    .createInstance(Components.interfaces.nsIAbCard);
                        card.displayName = iv.value.displayName;
                        card.primaryEmail = iv.value.email;
                        self.onSearchFoundCard(card);
                    }
                }
                self.onSearchFinished(Components.results.NS_OK);
            },
            function(aRejectReason) {
                self._logger.error(aRejectReason);
                self.onSearchFinished(Components.results.NS_ERROR_FAILURE, aRejectReason);
            }).catch(function(err) {
                self._logger.error(err);
                self.onSearchFinished(Components.results.NS_ERROR_FAILURE, err);
            });
        } else {
            this.onSearchFinished(Components.results.NS_OK);
        }
    },
    stopSearch: function() {
        if (this._searching) {
            this._searching = false;
        }
    },
    onSearchFoundCard: function(/*nsIAbCard*/ aCard) {
        if (!this._searching) return;
        if (this._searchListener) {
            this._searchListener.onSearchFoundCard(aCard, this.URI);
        }
    },
    onSearchFinished: function(aResult, aErrorMsg) {
        if (!this._searching) return;
        this._searching = false;
        if (this._searchListener) {
            this._searchListener.onSearchFinished(aResult, aErrorMsg, this.URI);
        }
    },
}


let NSGetFactory = XPCOMUtils.generateNSGetFactory([BmAutocompleteSearch]);
