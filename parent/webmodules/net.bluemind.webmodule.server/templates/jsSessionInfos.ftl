
      var bmcSessionInfos = { 
        "login": "${(BMUserLATD)!}",
        "accountType": "${(BMAccountType)!}",
        "defaultEmail": "${BMUserDefaultEmail!""}",
        "sid": "${(BMSessionId)!}",
        "userId": "${(BMUserId)!}",
        "hasIM": "${(BMHasIM)!}",
        "lang": "${(BMLang)!}",
        "domain": "${(BMUserDomainId)!}",
        "roles": "${(BMRoles)!}",
        "formatedName": "${(BMUserFormatedName)!}",
        "bmVersion" : "${(version)!}",
        "bmBrandVersion" : "${(brandVersion)!}",
      };

window['bmcSessionInfos'] = bmcSessionInfos;