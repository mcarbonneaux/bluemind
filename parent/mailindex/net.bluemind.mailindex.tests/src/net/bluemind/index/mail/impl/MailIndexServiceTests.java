/* BEGIN LICENSE
 * Copyright © Blue Mind SAS, 2012-2016
 *
 * This file is part of BlueMind. BlueMind is a messaging and collaborative
 * solution.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of either the GNU Affero General Public License as
 * published by the Free Software Foundation (version 3 of the License).
 *
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See LICENSE.txt
 * END LICENSE
 */
package net.bluemind.index.mail.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.apache.james.mime4j.MimeIOException;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.junit.Test;

import com.google.common.collect.Sets;
import com.google.common.io.Files;

import net.bluemind.backend.mail.api.MailboxFolderSearchQuery;
import net.bluemind.backend.mail.api.MailboxItem.SystemFlag;
import net.bluemind.backend.mail.api.SearchQuery;
import net.bluemind.backend.mail.api.SearchQuery.SearchScope;
import net.bluemind.backend.mail.api.SearchResult;
import net.bluemind.backend.mail.replica.indexing.MailSummary;
import net.bluemind.core.container.model.ItemValue;
import net.bluemind.index.MailIndexActivator;
import net.bluemind.lib.elasticsearch.ESearchActivator;
import net.bluemind.mailbox.api.ShardStats;

public class MailIndexServiceTests extends AbstractSearchTests {

	@Test
	public void testSimpleSearch() throws MimeIOException, IOException, InterruptedException, ExecutionException {
		Client c = ESearchActivator.getClient();
		long imapUid = 1;

		byte[] eml = Files.toByteArray(new File("data/test.eml"));
		storeBody(bodyUid, eml);
		storeMessage(mboxUid, userUid, bodyUid, imapUid, Collections.emptyList());
		ESearchActivator.refreshIndex(INDEX_NAME);

		SearchResponse resp = c.prepareSearch(INDEX_NAME)
				.setQuery(QueryBuilders.queryStringQuery("id:\"" + entryId(imapUid) + "\"")).execute().get();
		assertEquals(1L, resp.getHits().getTotalHits());

		Map<String, Object> source = resp.getHits().getAt(0).getSourceAsMap();
		assertEquals(bodyUid, source.get("parentId"));
		assertEquals("SubjectTest", source.get("subject"));
		assertEquals(entryId(imapUid), source.get("id"));

		// search by IN in alias
		resp = c.prepareSearch(ALIAS).setQuery(QueryBuilders.termQuery("in", folderUid)).execute().get();
		assertEquals(1L, resp.getHits().getTotalHits());

		source = resp.getHits().getAt(0).getSourceAsMap();
		assertEquals(bodyUid, source.get("parentId"));
		assertEquals("SubjectTest", source.get("subject"));
		assertEquals(entryId(imapUid), source.get("id"));
	}

	@Test
	public void testDelete() throws MimeIOException, IOException, InterruptedException, ExecutionException {
		Client c = ESearchActivator.getClient();
		long imapUid = 1;

		byte[] eml = Files.toByteArray(new File("data/test.eml"));
		storeBody(bodyUid, eml);
		storeMessage(mboxUid, userUid, bodyUid, imapUid, Collections.emptyList());
		ESearchActivator.refreshIndex(INDEX_NAME);

		SearchResponse resp = c.prepareSearch(INDEX_NAME)
				.setQuery(QueryBuilders.queryStringQuery("id:\"" + entryId(imapUid) + "\"")).execute().get();
		assertEquals(1L, resp.getHits().getTotalHits());

		List<SystemFlag> deleteFlag = new ArrayList<>();
		deleteFlag.add(SystemFlag.deleted);
		storeMessage(mboxUid, userUid, bodyUid, imapUid, deleteFlag);
		ESearchActivator.refreshIndex(INDEX_NAME);
		MailIndexActivator.getService().expunge("testbm.lan@bm.loc", ItemValue.create(userUid, null),
				ItemValue.create(folderUid, null));
		ESearchActivator.refreshIndex(INDEX_NAME);

		resp = c.prepareSearch(ALIAS).setQuery(QueryBuilders.queryStringQuery("id:\"" + entryId(imapUid) + "\""))
				.execute().get();
		assertEquals(0L, resp.getHits().getTotalHits());
	}

	@Test
	public void testBigExpunge() throws MimeIOException, IOException, InterruptedException, ExecutionException {
		Client c = ESearchActivator.getClient();

		byte[] eml = Files.toByteArray(new File("data/test.eml"));
		storeBody(bodyUid, eml);
		storeMessage(mboxUid, userUid, bodyUid, 1l, Collections.emptyList());
		storeMessage(mboxUid, userUid, bodyUid, 2l, Collections.emptyList());
		ESearchActivator.refreshIndex(INDEX_NAME);

		SearchResponse resp = c.prepareSearch(INDEX_NAME)
				.setQuery(QueryBuilders.queryStringQuery("id:\"" + entryId(1) + "\"")).execute().get();
		assertEquals(1L, resp.getHits().getTotalHits());
		resp = c.prepareSearch(INDEX_NAME).setQuery(QueryBuilders.queryStringQuery("id:\"" + entryId(2) + "\""))
				.execute().get();
		assertEquals(1L, resp.getHits().getTotalHits());

		Set<Integer> ordered = Sets.newLinkedHashSet();
		ordered.add(-1); // does not exist
		ordered.add(1);
		ordered.add(2);

		MailIndexActivator.getService().cleanupFolder(ItemValue.create(userUid, null),
				ItemValue.create(folderUid, null), ordered);

		resp = c.prepareSearch(INDEX_NAME).setQuery(QueryBuilders.queryStringQuery("id:\"" + entryId(1) + "\""))
				.execute().get();
		assertEquals(0L, resp.getHits().getTotalHits());
		resp = c.prepareSearch(INDEX_NAME).setQuery(QueryBuilders.queryStringQuery("id:\"" + entryId(2) + "\""))
				.execute().get();
		assertEquals(0L, resp.getHits().getTotalHits());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testUpdateFlags() throws MimeIOException, IOException, InterruptedException, ExecutionException {
		Client c = ESearchActivator.getClient();

		byte[] eml = Files.toByteArray(new File("data/test.eml"));
		storeBody(bodyUid, eml);
		storeMessage(mboxUid, userUid, bodyUid, 1, Collections.emptyList());
		storeMessage(mboxUid, userUid, bodyUid, 2, Collections.emptyList());
		ESearchActivator.refreshIndex(INDEX_NAME);

		List<MailSummary> mails = new ArrayList<>();
		MailSummary summary1 = new MailSummary();
		summary1.uid = 1;
		summary1.parentId = bodyUid;
		summary1.flags = new HashSet<>(Arrays.asList("flag1", "flag2"));
		MailSummary summary2 = new MailSummary();
		summary2.uid = 2;
		summary2.parentId = bodyUid;
		summary2.flags = new HashSet<>(Arrays.asList("flag2", "flag3"));

		mails.add(summary1);
		mails.add(summary2);

		MailIndexActivator.getService().syncFlags(ItemValue.create(userUid, null), ItemValue.create(folderUid, null),
				mails);
		ESearchActivator.refreshIndex(INDEX_NAME);

		SearchResponse resp = c.prepareSearch(INDEX_NAME)
				.setQuery(QueryBuilders.queryStringQuery("id:\"" + entryId(1) + "\"")).execute().get();
		assertEquals(1L, resp.getHits().getTotalHits());
		List<String> flags = (List<String>) resp.getHits().getAt(0).getSourceAsMap().get("is");
		assertTrue(flags.contains("flag1"));
		assertTrue(flags.contains("flag2"));

		resp = c.prepareSearch(INDEX_NAME).setQuery(QueryBuilders.queryStringQuery("id:\"" + entryId(2) + "\""))
				.execute().get();
		assertEquals(1L, resp.getHits().getTotalHits());
		flags = (List<String>) resp.getHits().getAt(0).getSourceAsMap().get("is");
		assertTrue(flags.contains("flag2"));
		assertTrue(flags.contains("flag3"));
	}

	@Test
	public void testFolders() throws MimeIOException, IOException {

		byte[] eml = Files.toByteArray(new File("data/test.eml"));
		storeBody(bodyUid, eml);
		String f1 = UUID.randomUUID().toString();
		String f2 = UUID.randomUUID().toString();
		storeMessage(f1, userUid, bodyUid, 1, Collections.emptyList());
		storeMessage(f2, userUid, bodyUid, 2, Collections.emptyList());
		ESearchActivator.refreshIndex(INDEX_NAME);

		Set<String> folders = MailIndexActivator.getService().getFolders(userUid);
		assertTrue(folders.contains(f1));
		assertTrue(folders.contains(f2));
	}

	@Test
	public void testMoveIndexToNewIndex() throws Exception {
		Client c = ESearchActivator.getClient();

		byte[] eml = Files.toByteArray(new File("data/test.eml"));
		storeBody(bodyUid, eml);
		storeMessage(mboxUid, userUid, bodyUid, 1, Collections.emptyList());
		storeMessage(mboxUid, userUid, bodyUid, 2, Collections.emptyList());
		ESearchActivator.refreshIndex(INDEX_NAME);

		MailIndexActivator.getService().moveMailbox(userUid, "mailspool_test2");
		ESearchActivator.refreshIndex(INDEX_NAME);

		SearchResponse resp = c.prepareSearch("mailspool_test2")
				.setQuery(QueryBuilders.queryStringQuery("id:\"" + entryId(1) + "\"")).execute().get();
		assertEquals(1L, resp.getHits().getTotalHits());

		resp = c.prepareSearch(ALIAS).setQuery(QueryBuilders.queryStringQuery("id:\"" + entryId(1) + "\"")).execute()
				.get();
		assertEquals(1L, resp.getHits().getTotalHits());

		resp = c.prepareSearch(INDEX_NAME).setQuery(QueryBuilders.queryStringQuery("id:\"" + entryId(1) + "\""))
				.execute().get();
		assertEquals(0L, resp.getHits().getTotalHits());
	}

	@Test
	public void testGetStats() throws MimeIOException, IOException {

		byte[] eml = Files.toByteArray(new File("data/test.eml"));
		storeBody(bodyUid, eml);
		storeMessage(mboxUid, userUid, bodyUid, 1, Collections.emptyList());
		storeMessage(mboxUid, userUid, bodyUid, 2, Collections.emptyList());
		ESearchActivator.refreshIndex(INDEX_NAME);

		List<ShardStats> stats = MailIndexActivator.getService().getStats();
		assertNotNull(stats);
		MailIndexActivator.getService().moveMailbox(userUid, "mailspool_test2");
		ESearchActivator.refreshIndex(INDEX_NAME);

		stats = MailIndexActivator.getService().getStats();
		assertNotNull(stats);
	}

	@Test
	public void testSearch() throws MimeIOException, IOException, InterruptedException, ExecutionException {
		long imapUid = 1;
		byte[] eml = Files.toByteArray(new File("data/test.eml"));
		storeBody(bodyUid, eml);
		storeMessage(mboxUid, userUid, bodyUid, imapUid, Collections.emptyList());
		ESearchActivator.refreshIndex(INDEX_NAME);

		SearchQuery query = new SearchQuery();
		query.maxResults = 10;
		query.offset = 0;
		query.query = "SubjectTest";
		query.scope = new SearchScope();
		query.scope.isDeepTraversal = true;
		MailboxFolderSearchQuery q = new MailboxFolderSearchQuery();
		q.query = query;
		SearchResult results = MailIndexActivator.getService().searchItems(userUid, q);

		assertEquals(1, results.totalResults);
		assertEquals("mbox_records_" + mboxUid, results.results.get(0).containerUid);
		assertEquals(44l, results.results.get(0).itemId);
		assertEquals("IPM.Note", results.results.get(0).messageClass);
	}

}
