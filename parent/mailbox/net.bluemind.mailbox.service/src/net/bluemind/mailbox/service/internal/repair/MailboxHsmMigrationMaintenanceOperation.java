/* BEGIN LICENSE
  * Copyright © Blue Mind SAS, 2012-2018
  *
  * This file is part of BlueMind. BlueMind is a messaging and collaborative
  * solution.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of either the GNU Affero General Public License as
  * published by the Free Software Foundation (version 3 of the License).
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  *
  * See LICENSE.txt
  * END LICENSE
  */
package net.bluemind.mailbox.service.internal.repair;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.bluemind.backend.mail.api.MailboxFolder;
import net.bluemind.backend.mail.replica.api.IDbReplicatedMailboxes;
import net.bluemind.backend.mail.replica.indexing.IDSet;
import net.bluemind.config.InstallationId;
import net.bluemind.core.api.fault.ServerFault;
import net.bluemind.core.api.report.DiagnosticReport;
import net.bluemind.core.container.model.ItemValue;
import net.bluemind.core.context.SecurityContext;
import net.bluemind.core.rest.BmContext;
import net.bluemind.core.rest.ServerSideServiceProvider;
import net.bluemind.core.task.service.IServerTaskMonitor;
import net.bluemind.hsm.api.IHSM;
import net.bluemind.hsm.api.Promote;
import net.bluemind.imap.Flag;
import net.bluemind.imap.IMAPException;
import net.bluemind.imap.SearchQuery;
import net.bluemind.imap.StoreClient;
import net.bluemind.imap.Summary;
import net.bluemind.index.mail.Sudo;
import net.bluemind.mailbox.api.Mailbox;
import net.bluemind.mailbox.api.Mailbox.Routing;
import net.bluemind.mailbox.service.internal.repair.MailboxRepairSupport.MailboxMaintenanceOperation;
import net.bluemind.server.api.IServer;
import net.bluemind.server.api.Server;

public class MailboxHsmMigrationMaintenanceOperation extends MailboxMaintenanceOperation {

	private static final Logger logger = LoggerFactory.getLogger(MailboxHsmMigrationMaintenanceOperation.class);
	private static final String MAINTENANCE_OPERATION_ID = DiagnosticReportCheckId.mailboxHsm.name();

	public MailboxHsmMigrationMaintenanceOperation(BmContext context) {
		super(context, MAINTENANCE_OPERATION_ID, null, "replication.subtree");
	}

	@Override
	protected void checkMailbox(String domainUid, DiagnosticReport report, IServerTaskMonitor monitor) {
		checkAndRepair(false, domainUid, report, monitor);
	}

	@Override
	protected void repairMailbox(String domainUid, DiagnosticReport report, IServerTaskMonitor monitor) {
		checkAndRepair(true, domainUid, report, monitor);
	}

	private void checkAndRepair(boolean repair, String domainUid, DiagnosticReport report, IServerTaskMonitor monitor) {
		monitor.begin(1, String.format("Check mailbox %s HSM migration", mailboxToString(domainUid)));

		if (mailbox.value.type.sharedNs) {
			logger.info("Mailbox {} ({}), type {} not supported", mailbox.uid, mailbox.value.name, mailbox.value.type);
			monitor.end(true, null, null);
			return;
		}

		if (!hsmCompleted(domainUid)) {
			traverseFolders(mailbox, domainUid, monitor);
			if (repair) {
				markAsFinished(domainUid);
			}
		}

		monitor.progress(1, String.format("Mailbox %s HSM migration finished", mailboxToString(domainUid)));
		report.ok(MAINTENANCE_OPERATION_ID,
				String.format("Mailbox %s HSM migration finished", mailboxToString(domainUid)));

		monitor.end(true, null, null);
	}

	private void markAsFinished(String domainUid) {
		String dir = String.format("/var/spool/bm-hsm/snappy/user/%s/%s", domainUid, mailbox.uid);
		new File(dir).mkdirs();

		try {
			getMarkerFile(domainUid).createNewFile();
		} catch (Exception e) {
			logger.warn("Cannot create hsm marker file {}", getMarkerFile(domainUid).getAbsolutePath(), e);
		}
	}

	private boolean hsmCompleted(String domainUid) {
		return getMarkerFile(domainUid).exists();
	}

	private File getMarkerFile(String domain) {
		return new File(
				String.format("/var/spool/bm-hsm/snappy/user/%s/%s/hsm.promote.completed", domain, mailbox.uid));
	}

	private void traverseFolders(ItemValue<Mailbox> mailbox, String domainUid, IServerTaskMonitor monitor)
			throws ServerFault {
		logger.info("Traversing folders of mailbox {} type {}, routing: {}", mailbox.displayName, mailbox.value.type,
				mailbox.value.routing);
		ItemValue<Server> server = ServerSideServiceProvider.getProvider(SecurityContext.SYSTEM)
				.instance(IServer.class, InstallationId.getIdentifier()).getComplete(mailbox.value.dataLocation);
		if (mailbox.value.routing == Routing.internal && !mailbox.value.archived) {
			IDbReplicatedMailboxes mbService = ServerSideServiceProvider.getProvider(SecurityContext.SYSTEM).instance(
					IDbReplicatedMailboxes.class, domainUid,
					mailbox.value.type.nsPrefix + mailbox.value.name.replace(".", "^"));
			List<ItemValue<MailboxFolder>> folders = mbService.all();

			monitor.log("Found " + folders.size() + " for " + mailbox.value.name.replace(".", "^"));
			logger.info("Found {} folders for ns {}{} on domain {}", folders.size(), mailbox.value.type.nsPrefix,
					mailbox.value.name.replace(".", "^"), domainUid);

			try (Sudo pass = new Sudo(mailbox.value.name, domainUid)) {
				IHSM hsm = ServerSideServiceProvider.getProvider(pass.context).instance(IHSM.class, domainUid);
				promote(domainUid, folders, hsm, pass, server, monitor);
			}
		}
	}

	private void promote(String domainUid, List<ItemValue<MailboxFolder>> folders, IHSM hsm, Sudo pass,
			ItemValue<Server> server, IServerTaskMonitor monitor) {

		try (StoreClient sc = new StoreClient(server.value.address(), 1143, mailbox.value.name + "@" + domainUid,
				pass.context.getSessionId())) {
			if (sc.login()) {
				for (ItemValue<MailboxFolder> folder : folders) {
					logger.info("Checking folder {} ", folder.value.fullName);
					promoteFolder(hsm, monitor, sc, folder);
				}
			}
		}
	}

	private void promoteFolder(IHSM hsm, IServerTaskMonitor monitor, StoreClient sc, ItemValue<MailboxFolder> folder) {
		try {
			sc.select(folder.value.fullName);
		} catch (IMAPException e) {
			logger.warn("Cannot select folder {}", folder.value.fullName, e);
			return;
		}
		SearchQuery sq = new SearchQuery();
		sq.setKeyword(Flag.BMARCHIVED.toString());
		Collection<Integer> archived = sc.uidSearch(sq);
		IDSet idset = IDSet.create(archived.iterator());

		logger.info("Found {} archived entries in folder {} ", archived.size(), folder.displayName);
		monitor.log(archived.size() + " archived messages in folder " + folder.value.fullName);

		if (!archived.isEmpty()) {
			idset.forEach(idRange -> {

				logger.info("Promoting from {}  to {}", idRange.from(), idRange.to());

				if (idRange.from() > 0) {
					String smallerRange = idRange.toString();
					Collection<Summary> imapSummaries = sc.uidFetchSummary(smallerRange);

					logger.info("Promoting {} summaries", imapSummaries.size());

					promoteSummaries(mailbox, folder, imapSummaries, hsm);
				}
			});
		}
	}

	private void promoteSummaries(ItemValue<Mailbox> mailbox, ItemValue<MailboxFolder> folder,
			Collection<Summary> imapSummaries, IHSM hsm) {

		List<Promote> toPromote = imapSummaries.stream().map(s -> summaryToPromote(s, folder, mailbox.uid))
				.collect(Collectors.toList());

		hsm.promoteMultiple(toPromote);

	}

	private Promote summaryToPromote(Summary sum, ItemValue<MailboxFolder> folder, String mailboxUid) {
		Promote promote = new Promote();
		promote.folder = folder.value.fullName;
		promote.imapUid = sum.getUid();
		promote.mailboxUid = mailboxUid;
		promote.hsmId = sum.getHeaders().getRawHeader("X-BM_HSM_ID");
		promote.internalDate = sum.getDate();
		promote.flags = sum.getFlags().asTags();

		return promote;
	}

}
