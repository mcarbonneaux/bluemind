/* BEGIN LICENSE
  * Copyright © Blue Mind SAS, 2012-2016
  *
  * This file is part of BlueMind. BlueMind is a messaging and collaborative
  * solution.
  *
  * This program is free software; you can redistribute it and/or modify
  * it under the terms of either the GNU Affero General Public License as
  * published by the Free Software Foundation (version 3 of the License).
  *
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  *
  * See LICENSE.txt
  * END LICENSE
  */
package net.bluemind.system.service.internal;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertPath;
import java.security.cert.CertPathValidator;
import java.security.cert.CertPathValidatorException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.PKIXParameters;
import java.security.cert.TrustAnchor;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bouncycastle.asn1.pkcs.PrivateKeyInfo;
import org.bouncycastle.openssl.PEMKeyPair;
import org.bouncycastle.openssl.PEMParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.bluemind.config.InstallationId;
import net.bluemind.core.api.fault.ServerFault;
import net.bluemind.core.container.model.ItemValue;
import net.bluemind.core.container.service.internal.RBACManager;
import net.bluemind.core.rest.BmContext;
import net.bluemind.core.task.api.TaskRef;
import net.bluemind.core.task.service.ITasksManager;
import net.bluemind.node.api.INodeClient;
import net.bluemind.node.api.NCUtils;
import net.bluemind.node.client.AHCNodeClientFactory;
import net.bluemind.role.api.BasicRoles;
import net.bluemind.server.api.IServer;
import net.bluemind.server.api.Server;
import net.bluemind.system.api.CertData;
import net.bluemind.system.api.ISecurityMgmt;
import net.bluemind.system.hook.ISystemHook;
import net.bluemind.system.iptables.UpdateFirewallRulesTask;

public class SecurityMgmt implements ISecurityMgmt {
	private static final Logger logger = LoggerFactory.getLogger(SecurityMgmt.class);
	private BmContext context;
	private List<ISystemHook> hooks;
	private RBACManager rbac;

	public SecurityMgmt(BmContext context, List<ISystemHook> hooks) {
		this.context = context;
		this.hooks = hooks;
		rbac = new RBACManager(context);
	}

	@Override
	public TaskRef updateFirewallRules() throws ServerFault {
		rbac.check(BasicRoles.ROLE_MANAGE_SYSTEM_CONF);

		return context.provider().instance(ITasksManager.class).run(new UpdateFirewallRulesTask());
	}

	@Override
	public void updateCertificate(CertData certData) throws ServerFault {
		rbac.check(BasicRoles.ROLE_MANAGE_SYSTEM_CONF);

		logger.info("update certificate by {}", context.getSecurityContext().getSubject());
		String certificateAuthority = certData.certificateAuthority;
		String certificate = certData.certificate;
		String privateKey = certData.privateKey;

		checkCertificate(certificateAuthority.getBytes(), certificate.getBytes(), privateKey.getBytes());

		IServer serverService = context.provider().instance(IServer.class, InstallationId.getIdentifier());

		for (ItemValue<Server> serverItem : serverService.allComplete()) {
			writeCert(serverItem.value, certificateAuthority, certificate, privateKey);
		}

		fireCertificateUpdated();
	}

	private void fireCertificateUpdated() throws ServerFault {
		for (ISystemHook hook : hooks) {
			hook.onCertificateUpdate();
		}
	}

	private void writeCert(Server server, String ca, String cert, String pkey) throws ServerFault {
		String certPlusKey = cert + "\n" + pkey + "\n" + ca;
		INodeClient nc = new AHCNodeClientFactory().create(server.address());
		copyCertToNode(nc, ca, certPlusKey);
	}

	private void copyCertToNode(INodeClient nc, String ca, String certPlusKey) throws ServerFault {
		TaskRef tr = nc.executeCommand("mkdir -p /var/lib/bm-ca");
		NCUtils.waitFor(nc, tr);
		nc.writeFile("/var/lib/bm-ca/cacert.pem", new ByteArrayInputStream(ca.getBytes()));

		tr = nc.executeCommand("mkdir -p /etc/bm/certs");
		NCUtils.waitFor(nc, tr);
		nc.writeFile("/etc/bm/certs/bm_cert.pem", new ByteArrayInputStream(certPlusKey.getBytes()));
		nc.writeFile("/etc/ssl/certs/bm_cert.pem", new ByteArrayInputStream(certPlusKey.getBytes()));
	}

	static public void checkCertificate(byte[] caData, byte[] certData, byte[] pkeyData) throws ServerFault {
		CertificateFactory cf = null;
		try {
			cf = CertificateFactory.getInstance("X.509");
		} catch (CertificateException e) {
			throw new ServerFault("CertificateFactory not available", e);
		}

		// loading CAs
		Collection<? extends Certificate> certificates = null;
		try {
			certificates = cf.generateCertificates(new ByteArrayInputStream(caData));

		} catch (CertificateException e) {
			logger.warn("error during ca read {}", e);
			throw new ServerFault("Certificate Authority not valid : " + e.getMessage(), e);
		}

		Set<TrustAnchor> tA = new HashSet<TrustAnchor>();
		List<X509Certificate> certList = new ArrayList<X509Certificate>();
		for (Certificate aCertificate : certificates) {
			X509Certificate certificate = (X509Certificate) aCertificate;

			if (!certificate.getSubjectDN().equals(certificate.getIssuerDN())
					&& certificate.getBasicConstraints() == -1) {
				throw new ServerFault("Certificate Authority is not one");
			}
			logger.info("CA issuer {} for {} depth {} ", certificate.getIssuerX500Principal(),
					certificate.getSubjectX500Principal(), certificate.getBasicConstraints());

			// add CA to trusted anchors
			tA.add(new TrustAnchor(certificate, null));
			certList.add(certificate);
		}

		// load certficate
		X509Certificate cert = null;
		try {
			cert = (X509Certificate) cf.generateCertificate(new ByteArrayInputStream(certData));
		} catch (CertificateException e) {
			logger.warn("error during cert read {}", e);
			throw new ServerFault("Certificate not valid : " + e.getMessage(), e);
		}

		// verify cert is valid against trused CAs (caData)
		try {
			CertPath cp = cf.generateCertPath(Arrays.asList(cert));

			// list of trusted CA (from caData)
			PKIXParameters pkixp = new PKIXParameters(tA);
			pkixp.setRevocationEnabled(false);

			CertPathValidator cpv = CertPathValidator.getInstance("PKIX");

			cpv.validate(cp, pkixp);
		} catch (CertificateException e) {
			logger.warn("error during cert validation {}", e);
			throw new ServerFault("Certificate not valid : " + e.getMessage(), e);
		} catch (InvalidAlgorithmParameterException e) {
			logger.warn("error during cert validation {}", e);
			throw new ServerFault("Certificate not valid : " + e.getMessage(), e);
		} catch (NoSuchAlgorithmException e) {
			logger.error("error during cert validation {}", e);
			throw new ServerFault("error during certificate validation", e);
		} catch (CertPathValidatorException e) {
			logger.warn("error during cert validation {}", e);
			throw new ServerFault("Certificate path not valid : " + e.getMessage(), e);
		}

		logger.info("Certificate issuer {} for {} ", cert.getIssuerX500Principal(), cert.getSubjectX500Principal());
		if (cert.getBasicConstraints() != -1) {
			// not a CA
			throw new ServerFault("Certificate is not a certificate but a CA");
		}

		// finally validate the key against certificate

		// load privatekey
		PrivateKey pk = null;
		try {
			KeyFactory keyFactory = KeyFactory.getInstance("RSA");

			@SuppressWarnings("resource")
			Object o = new PEMParser(new StringReader(new String(pkeyData))).readObject();
			PrivateKeyInfo privateKeyInfo = null;
			if (o instanceof PEMKeyPair) {
				PEMKeyPair keyPair = (PEMKeyPair) o;
				privateKeyInfo = keyPair.getPrivateKeyInfo();
			} else if (o instanceof PrivateKeyInfo) {
				privateKeyInfo = (PrivateKeyInfo) o;
			} else if (o == null) {
				throw new ServerFault("privatekey format not handled ");
			} else {
				throw new ServerFault("privatekey format not handled " + o.getClass().getName());
			}

			pk = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateKeyInfo.getEncoded()));
		} catch (NoSuchAlgorithmException e) {
			logger.warn("error during pk validation {}", e);
			throw new ServerFault("error during private key validation ", e);
		} catch (InvalidKeySpecException e) {
			throw new ServerFault("error during private key loading : " + e.getMessage(), e);
		} catch (IOException e) {
			logger.warn("error during pk validation {}", e);
			throw new ServerFault("error during private key validation ", e);
		}

		try {
			Signature dsa = Signature.getInstance("SHA1withRSA");
			dsa.initSign(pk);
			dsa.update("testSign".getBytes());
			byte[] signature = dsa.sign();

			dsa = Signature.getInstance("SHA1withRSA");
			dsa.initVerify(cert.getPublicKey());
			dsa.update("testSign".getBytes());
			if (!dsa.verify(signature)) {
				throw new ServerFault("private key doesnt correspond to certificate");
			}

		} catch (SignatureException e) {
			logger.warn("error during pk validation {}", e);
			throw new ServerFault("error during private key validation : " + e.getMessage(), e);
		} catch (NoSuchAlgorithmException e) {
			logger.warn("error during pk validation {}", e);
			throw new ServerFault("error during private key validation : " + e.getMessage(), e);
		} catch (InvalidKeyException e) {
			logger.warn("error during pk validation {}", e);
			throw new ServerFault("error during private key validation : " + e.getMessage(), e);
		}

	}
}
