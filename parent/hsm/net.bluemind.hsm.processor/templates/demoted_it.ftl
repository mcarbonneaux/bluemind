<#include "head.ftl">
<h1>This message is archived</h1>
<p>You can still <a href="${archLink}">consult it</a>.</p>
<#include "foot.ftl">
